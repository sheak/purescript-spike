module Test.Data.WebLead where
  
import Prelude

import Data.Maybe (Maybe(..))
import Data.WebLead (ContactMethod(..), LeadDetails(..), WebLead(..))
import Foreign.Generic (encodeJSON)
import Test.Spec (Spec, describe, it)
import Test.Spec.Assertions (shouldEqual)

spec :: Spec Unit
spec =
  describe "WebLead" do
    it "should encode to json" do
      let 
        webLead = WebLead
          { id: "1"
          , leadDetails: LeadDetails 
            { name: "Jimmy Jones"
            , preferredContactMethod: Email "shea@sheakelly.com"
            , otherContactMethod: [ LandLine "0245674567" ]
            , message: Just "Hello there"  
            }
          }
      (encodeJSON webLead) `shouldEqual` ""