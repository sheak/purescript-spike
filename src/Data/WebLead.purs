module Data.WebLead where

import Prelude

import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Show (genericShow)
import Data.Maybe (Maybe)
import Foreign.Class (class Decode, class Encode)
import Foreign.Generic (defaultOptions, genericDecode, genericEncode)
import Foreign.Generic.Types (Options)

options :: Options
options = defaultOptions { unwrapSingleConstructors = true }

data ContactMethod 
  = Email String 
  | LandLine String 
  | Mobile String

derive instance genericContactMethod :: Generic ContactMethod _
instance showContactMethod :: Show ContactMethod where show = genericShow
instance decodeContactMethod :: Decode ContactMethod where 
  decode = genericDecode options
instance encodeContactMethod :: Encode ContactMethod where 
  encode = genericEncode options

newtype LeadDetails = LeadDetails
  { name :: String
  , preferredContactMethod :: ContactMethod 
  , otherContactMethod :: Array ContactMethod
  , message :: Maybe String
  }

derive instance genericLeadDetails :: Generic LeadDetails _
instance showLeadDetails :: Show LeadDetails where show = genericShow
instance decodeLeadDetails :: Decode LeadDetails where 
  decode = genericDecode options
instance encodeLeadDetails :: Encode LeadDetails where 
  encode = genericEncode options

newtype WebLead = WebLead
  { id :: String
  , leadDetails :: LeadDetails
  }

derive instance genericWebLead :: Generic WebLead _
instance showWebLead :: Show WebLead where show = genericShow
instance decodeWebLead :: Decode WebLead where 
  decode = genericDecode options
instance encodeWebLead :: Encode WebLead where 
  encode = genericEncode options