module Main where
  
import Prelude

import Control.Monad.Except (runExcept)
import Data.Either (Either(..))
import Data.Function.Uncurried (Fn3)
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Show (genericShow)
import Effect (Effect)
import Foreign (F, renderForeignError)
import Foreign.Class (class Decode)
import Foreign.Generic (defaultOptions, genericDecode)
import Foreign.Generic.Types (Options)
import Network.AWS.Lambda.Express as Lambda
import Node.Express.App (App, get, post, useExternal, listenHttp)
import Node.Express.Handler (Handler, HandlerM)
import Node.Express.Request (getBody)
import Node.Express.Response (sendJson)
import Node.Express.Types (Response, Request)
import Node.HTTP (Server)
import Effect.Console (log)
import Data.Foldable (intercalate)

foreign import jsonBodyParser :: Fn3 Request Response (Effect Unit) (Effect Unit)

decodeOptions :: Options
decodeOptions = defaultOptions { unwrapSingleConstructors = true } 

newtype LeadData = LeadData 
  { name :: String
  , postcode :: String 
  }

derive instance genericLeadData :: Generic LeadData _
instance showLeadData :: Show LeadData where show = genericShow
instance decodeLeadData :: Decode LeadData where 
  decode = genericDecode decodeOptions

indexHandler :: Handler
indexHandler = do
  sendJson { status: "ok" }

postLeadHandler :: Handler
postLeadHandler = 
  (getBody :: HandlerM (F LeadData)) >>= \result ->
    case runExcept result of
      Left errs -> sendJson { error: intercalate ", " $ map renderForeignError errs}
      Right _ -> sendJson { status: "right" }

app :: App
app = do
  useExternal jsonBodyParser
  get "/" indexHandler
  post "/api/lead" postLeadHandler

main :: Effect Server
main = do
    listenHttp app 8080 \_ ->
        log $ "Listening on " <> show 8080

handler :: Lambda.HttpHandler
handler =
  Lambda.makeHandler app